﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace NoteBook1
{
    public class ServicePrice
    {
        public Guid Id { get; set; }
        public double Price { get; set; }
    }
    public class Prices
    {
        public ServicePrice[] ServicePrices { get; set; }
    }
    public class Program
    {
        static void Main()
        {
            #region Закоментированная ебала
            //var serviceRepository = new ServiceRepository();
            //serviceRepository.Create("Протезирование", "Хз что такое, звучит круто");
            //serviceRepository.Create("Установка коронки", "Долго объяснять, просто дай денег.");
            //serviceRepository.Create("Пульпи", "Лечение пульпитного зуба");
            //serviceRepository.Create("Кариес", "Лечение кариозного зуба");
            //serviceRepository.Create("Чистка", "Ультразвуковое удаление зубных отложений");
            //Environment.Exit(0);

            //var employee1 = new EmployeeRepository();
            //employee1.Create("Анастэзиолог", "Кикабидзе", "Абдул", "Салам", new DateTime(2000, 07, 15), "rightHook.na", "8-922-684-53-52");
            //employeeRepository.Remove(new Guid("a6332d6d-0b6e-4965-b572-b97644845e27"));

            //var pacient = new PacientRepository();
            //pacient.Create("Петрова", "Мария", "Ивановна", new DateTime(1992, 07, 20), "stereotipe@.nunah", "8-950-812-96-85");

            //var reception = new ReceptionRepository();
            //reception.Create
            //    (
            //    new Guid("394685d6-65b3-41ec-bb1f-927fa6f9b497"),
            //    new Guid("0fdd70d1-c9b9-45cf-814f-8d53b5871daa"),
            //    new DateTime(2021, 03, 13, 10, 30, 0),
            //    new DateTime(2021, 03, 13, 12, 00, 0)
            //    );

            //var receptionService = new ReceptionServiceRepository();
            //receptionService.Create
            //    (
            //    new Guid("d5d47bed-89ea-4d02-bfc1-af9629bb1fc7"),
            //    new Guid("135bad49-f602-4973-b65b-c46d8b8478c6"),
            //    100500.50
            //    );




            //servPrice.Add(new Guid("135bad49-f602-4973-b65b-c46d8b8478c6"), 300);
            //servPrice.Add(new Guid("361a3642-3ecb-450e-a660-7a44cbf5e6b4"), 100);
            //servPrice.Add(new Guid("44d4db9a-1a2e-4cca-9114-8501e7712338"), 5000);
            //servPrice.Add(new Guid("5c349d42-7ada-4e1b-ad89-2c304801bd74"), 2000);
            //servPrice.Add(new Guid("810ccbc1-2727-41f7-a72a-49e31a3e43ee"), 1000); 
            //servPrice.Add(new Guid("9671b69e-aba9-4dac-bd60-5a4016726764"), 200);
            //servPrice.Add(new Guid("f6db6b26-9a48-4af8-8091-02dc7c9fe94b"), 12000);
            #endregion

            var prices = new Prices();
            using (StreamReader fs = new StreamReader(@"D:\BitBucket\Notebook2\master\NoteBook1\serviceIdAndPrice.txt"))
            {
                var text = fs.ReadToEnd();
                Prices costs = JsonSerializer.Deserialize<Prices>(text);

                Dictionary<Guid, double> servPrice = new Dictionary<Guid, double>();
                servPrice.Add(new Guid(Convert.ToString(costs.ServicePrices[0].Id)), costs.ServicePrices[0].Price);
                servPrice.Add(new Guid(Convert.ToString(costs.ServicePrices[3].Id)), costs.ServicePrices[3].Price);
                servPrice.Add(new Guid(Convert.ToString(costs.ServicePrices[6].Id)), costs.ServicePrices[6].Price);

                var reception = new ReceptionProvider();
                reception.Create
                    (
                    new Guid("394685d6-65b3-41ec-bb1f-927fa6f9b497"),
                    new Guid("0fdd70d1-c9b9-45cf-814f-8d53b5871daa"),
                    new DateTime(2021, 03, 13, 10, 30, 0),
                    new DateTime(2021, 03, 13, 12, 00, 0),
                    servPrice
                    );
            }
        }
    }
}
