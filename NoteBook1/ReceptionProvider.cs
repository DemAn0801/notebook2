﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using NoteBook1.Entity;
using System.Text;

namespace NoteBook1
{
    class ReceptionProvider
    {
        public void Create
            (
            Guid employeeId,
            Guid pacientId,
            DateTime receptionStart,
            DateTime receptionEnd,
            Dictionary<Guid, double> services
            )
        {
            var employeeRepository = new EmployeeRepository();
            var pacientRepository = new PacientRepository();
            var receptionRepository = new ReceptionRepository();
            var receptionServiceRepository = new ReceptionServiceRepository();
            var serviceRepository = new ServiceRepository();

            if (!services.Any())
                throw new ArgumentException("Services is empty.");

            if (employeeRepository.GetById(employeeId) == Guid.Empty)
                throw new ArgumentException("Employee is not found in database.");

            if (pacientRepository.GetById(pacientId) == Guid.Empty)
                throw new ArgumentException("Pacient is not found in database.");


            foreach (var key in services.Keys)
            {
                if (serviceRepository.GetById(key) == Guid.Empty)
                    throw new ArgumentException("Service is not exist.");
            }
                var receptionId = receptionRepository.Create(employeeId, pacientId, receptionStart, receptionEnd);
         
            foreach (var key in services.Keys)
            {
                var receptionServiceId = receptionServiceRepository.Create(receptionId, key, services[key]);
                Console.WriteLine(receptionServiceId);
            }
        }
    }
}







