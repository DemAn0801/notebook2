﻿using System;
using NoteBook1.Entity;
using System.Collections.Generic;
using System.Text;

namespace NoteBook1.Entity
{
    public class PacientEntity
    {
        public Guid Id { get; set; }
        public string PacientSurname { get; set; }
        public string PacientFirstName { get; set; }
        public string PacientSecondName { get; set; }
        public DateTime PacientBirthDate { get; set; }
        public string PacientEmail { get; set; }
        public string PacientMobilePhone { get; set; }
    }
}
