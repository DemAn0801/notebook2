﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoteBook1
{
    public class ReceptionEntity
    {
        public Guid Id { get; set; }
        public DateTime ReceptionStart { get; set; }
        public DateTime ReceptionEnd { get; set; }
        public Guid EmployeeId { get; set; }
        public Guid PacientId { get; set; }
       


    }
}
