﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoteBook1
{
    public class ReceptionServiceEntity
    {
        public Guid Id { get; set; }
        public Guid ReceptionId { get; set; }
        public Guid ServiceId { get; set; }
        public double Price { get; set; }
    }
}
