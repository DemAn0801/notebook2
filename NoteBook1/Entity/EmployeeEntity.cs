﻿using System;
using NoteBook1.Entity;
using System.Collections.Generic;
using System.Text;

namespace NoteBook1.Entity
{
    public class EmployeeEntity
    {
        public Guid Id { get; set; }
        public string EmployeePost { get; set; }
        public string EmployeeSurname { get; set; }
        public string EmployeeFirsName { get; set; }
        public string EmployeeSecondName { get; set; }
        public DateTime EmployeeBirthDate { get; set; }
        public string EmployeeEmail { get; set; }
        public string EmployeeMobilePhone { get; set; }

    }
}
