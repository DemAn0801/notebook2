﻿using Microsoft.EntityFrameworkCore;
using NoteBook1.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace NoteBook1
{
    class DataBaseContext:DbContext
    {
        public DbSet<EmployeeEntity> Employees { get; set; }
        public DbSet<PacientEntity> Pacients { get; set; }
        public DbSet<ReceptionEntity> Receptions { get; set; }
        public DbSet<ReceptionServiceEntity> ReceptionsServices { get; set; }
        public DbSet<ServiceEntity> Services { get; set; }
        public DataBaseContext()
        {
            Database.EnsureCreated();
        }
        
        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    var employee1 = new EmployeeEntity { id = Guid.NewGuid(), EmployeePost = "Зубной врач", EmployeeFirsName = "Бедрос", EmployeeSecondName = "Аркадич", EmployeeSurname = "Грушевский", EmployeeBirthDate = new DateTime(1972, 12 ,20), EmployeeEmail = "pedro@huyandex.ssu" , EmployeeMobilePhone="+7952-800-56-32"};
        //     var employee2 = new EmployeeEntity { id = Guid.NewGuid(), EmployeePost = "Ассистент", EmployeeFirsName = "Вазген", EmployeeSecondName = "Зурабович", EmployeeSurname = "Петросян", EmployeeBirthDate = new DateTime(1972, 10, 5), EmployeeEmail = "ariec@huyandex.ssu", EmployeeMobilePhone = "+7952-800-56-15" };
        //    var pacient1 = new PacientEntity { Id = Guid.NewGuid(), PacientFirstName = "Педро", PacientSecondName = "Варфаламеевич", PacientSurname = "Паскаль", PacientBirthDate = new DateTime(1982, 11, 5), PacientEmail = "bdnkdfbjildfbjl@njfbnfbnk.ru", PacientMobilePhone = "4546467683" };
        //    var service1 = new ServiceEntity { Id = Guid.NewGuid(), ServiceTitle = "Отбеливание ануса", ServiceDiscription = "Бурый калечко тебе блестячий сделаю, да!Ауф Жиесть!" };

        //    var reception1 = new ReceptionEntity()
        //    {
        //        Id = Guid.NewGuid(),
        //        EmployeeId = employee1.id,
        //        PacientId = pacient1.Id,
        //        ReceptionStart = new DateTime(2015, 7, 20, 10, 30, 0),
        //        ReceptionEnd = new DateTime(2015, 7, 20, 11, 30, 0),

        //    };

        //    modelBuilder.Entity<ReceptionEntity>().HasData(reception1);

        //    var reseptionService1 = new ReceptionServiceEntity()
        //    {
        //        Id = Guid.NewGuid(),
        //        ReceptionId = reception1.Id,
        //        ServiceId = service1.Id,
        //        Price = 100500
        //    };

        //    modelBuilder.Entity<ReceptionServiceEntity>().HasData(reseptionService1);
        //}
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=Dentistry;Username=postgres;Password=534493Aa");
        }

    } 
}
