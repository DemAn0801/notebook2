﻿using System;
using NoteBook1.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoteBook1
{
    public class PacientRepository : PacientEntity
    {
        public void Create
            (
            string surname,
            string firstName,
            string secondName,
            DateTime birthDate,
            string email,
            string mobilePhone
            )
        {
            var dbRecord = new PacientEntity();
            dbRecord.Id = Guid.NewGuid();
            dbRecord.PacientSurname = surname;
            dbRecord.PacientFirstName = firstName;
            dbRecord.PacientSecondName = secondName;
            dbRecord.PacientBirthDate = birthDate;
            dbRecord.PacientEmail = email;
            dbRecord.PacientMobilePhone = mobilePhone;
            using (var db = new DataBaseContext())
            {
                db.Pacients.Add(dbRecord);
                db.SaveChanges();
            }
        }
        public void Remove(Guid id)
        {
            using (var db = new DataBaseContext())
            {
                var record = db.Pacients.Find(id);
                db.Pacients.Remove(record);
                db.SaveChanges();
            }
        }
        public IList<PacientEntity> GetAll()
        {
            using (var db = new DataBaseContext())
            {
                return db.Pacients.ToList();
            }
        }
        public Guid GetById(Guid id)
        {
            using (var dbPacients = new DataBaseContext())
            {
                var pacientId = dbPacients.Pacients.FirstOrDefault(x => x.Id == id);
                if (pacientId != null)
                    return pacientId.Id;
                else
                    return Guid.Empty;
                
            }
        }
    }
}
