﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoteBook1
{
    public class ServiceRepository:ServiceEntity 
    {
            public Guid Create(string title, string discription)
            {
                var dbRecord = new ServiceEntity();
                dbRecord.Id = Guid.NewGuid();
                dbRecord.ServiceTitle = title;
                dbRecord.ServiceDiscription = discription;
                using (var db = new DataBaseContext())
                {
                    db.Services.Add(dbRecord);
                    db.SaveChanges();
                }

            return dbRecord.Id;
            }
            public void Remove(Guid id)
            {
                using (var db = new DataBaseContext())
                {
                var record = db.Services.Find(id);
                    db.Services.Remove(record);
                    db.SaveChanges();
                }
            }
            public IList<ServiceEntity> GetAll()
            {
                var result = new List<ServiceEntity>();
                using (var db = new DataBaseContext())
                {
                    return db.Services.ToList();
                }
            }
        public Guid GetById (Guid id)
        {
            using (var dbServices = new DataBaseContext())
            {
                var serviceId = dbServices.Services.FirstOrDefault(x => x.Id == id);
                if (serviceId != null)
                    return serviceId.Id;
                else
                    return Guid.Empty;

            }
        }
        }
}
