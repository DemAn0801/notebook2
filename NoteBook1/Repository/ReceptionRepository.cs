﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace NoteBook1
{
    public class ReceptionRepository : ReceptionEntity
    {
        public Guid Create
            (
            Guid employeeId,
            Guid pacientId,
            DateTime receptionStart,
            DateTime receptionEnd
            )
        {
            var dbRecord = new ReceptionEntity();
            dbRecord.Id = Guid.NewGuid();
            dbRecord.ReceptionStart = receptionStart;
            dbRecord.ReceptionEnd = receptionEnd;
            dbRecord.EmployeeId = employeeId;
            dbRecord.PacientId = pacientId;
           
            using (var db = new DataBaseContext())
            {
                db.Receptions.Add(dbRecord);
                db.SaveChanges();
            }

            return dbRecord.Id;
        }
        public void Remove(Guid id)
        {
            using (var db = new DataBaseContext())
            {
                ReceptionEntity record = db.Receptions.Find(id);
                db.Receptions.Remove(record);
                db.SaveChanges();
            }
        }
        public IList<ReceptionEntity> GetAll()
        {
            var result = new List<ReceptionEntity>();
            using (var db = new DataBaseContext())
            {
                return db.Receptions.ToList();
            }
        }
    }
}
