﻿using NoteBook1.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoteBook1
{
    public class EmployeeRepository : EmployeeEntity
    {
        public void Create
            (
            string post,
            string surname,
            string firstName,
            string secondName,
            DateTime birthDate,
            string email,
            string mobilePhone
            )
        {
            var dbRecord = new EmployeeEntity();
            dbRecord.Id = Guid.NewGuid();
            dbRecord.EmployeePost = post;
            dbRecord.EmployeeSurname = surname;
            dbRecord.EmployeeFirsName = firstName;
            dbRecord.EmployeeSecondName = secondName;
            dbRecord.EmployeeBirthDate = birthDate;
            dbRecord.EmployeeEmail = email;
            dbRecord.EmployeeMobilePhone = mobilePhone;
            using (var db = new DataBaseContext())
            {
                db.Employees.Add(dbRecord);
                db.SaveChanges();
            }
        }
        public void Remove(Guid id)
        {
            using (var db = new DataBaseContext())
            {
                var record = db.Employees.Find(id);
                db.Employees.Remove(record);
                db.SaveChanges();
            }
        }
        public IList<EmployeeEntity> Read()
        {
            var result = new List<EmployeeEntity>();
            using (var db = new DataBaseContext())
            {
                var records = db.Employees.ToList();
                foreach (var record in records)
                {
                    var tempRecord = new EmployeeEntity();
                    tempRecord.Id = record.Id;
                    tempRecord.EmployeePost = record.EmployeePost;
                    tempRecord.EmployeeSurname = record.EmployeeSurname;
                    tempRecord.EmployeeFirsName = record.EmployeeFirsName;
                    tempRecord.EmployeeSecondName = record.EmployeeSecondName;
                    tempRecord.EmployeeBirthDate = record.EmployeeBirthDate;
                    tempRecord.EmployeeEmail = record.EmployeeEmail;
                    tempRecord.EmployeeMobilePhone = record.EmployeeMobilePhone;
                    result.Add(tempRecord);
                }
                return result;
            }
        }
        public Guid GetById(Guid id)
        {
            using (var dbEmployees = new DataBaseContext())
            {
                var employeeId = dbEmployees.Employees.FirstOrDefault(x => x.Id == id);
                if (employeeId != null)
                return employeeId.Id;
                else 
                return Guid.Empty;
            }
        }
    }
}

