﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoteBook1
{
    public class ReceptionServiceRepository:ReceptionServiceEntity
    {
        public Guid Create
            (
            Guid receptionId,
            Guid serviceId,
            double price
            )
        {
            var dbRecord = new ReceptionServiceEntity();
            dbRecord.Id = Guid.NewGuid();
            dbRecord.ReceptionId= receptionId;
            dbRecord.ServiceId= serviceId;
            dbRecord.Price = price;

            using (var db = new DataBaseContext())
            {
                db.ReceptionsServices.Add(dbRecord);
                db.SaveChanges();
            }

            return dbRecord.Id;
        }
        public void Remove(Guid id)
        {
            using (var db = new DataBaseContext())
            {
                var record = db.ReceptionsServices.Find(id);
                db.ReceptionsServices.Remove(record);
                db.SaveChanges();
            }
        }
        public IList<ReceptionEntity> GetAll()
        {
            var result = new List<ReceptionEntity>();
            using (var db = new DataBaseContext())
            {
                return db.Receptions.ToList();
            }
        }
    }
}
